import pika
import sys
import os


# Access AMQP_URL (fallback to localhost)
url = os.environ.get('AMQP_URL', 'amqp://guest:guest@localhost/%2F')
params = pika.URLParameters(url)
params.socket_timeout = 5

# connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', socket_timeout=5))
connection = pika.BlockingConnection(params)  # Connect to AMQP
channel = connection.channel()  # start a channel
channel.queue_declare(queue='task_queue', durable=True)  # Declare a queue

# send a message
message = ' '.join(sys.argv[1:]) or "Hello World!"
channel.basic_publish(exchange='', routing_key='task_queue', body=message,
                      properties=pika.BasicProperties(
                          delivery_mode=2,  # make message persistent
                      ))
print('[x] Sent "%r" to consumer' % message)
connection.close()
