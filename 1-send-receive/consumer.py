import pika
import time
import os

# Access the AMQP_URL (fallback to localhost)
url = os.environ.get('AMQP_URL', 'amqp://guest:guest@localhost/%2F')
params = pika.URLParameters(url)

# connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
connection = pika.BlockingConnection(params)  # Connect to AMQP
channel = connection.channel()  # start a channel
channel.queue_declare(queue='hello')

# create a function which is called on incoming messages
def callback(channel, method, properties, body):
    print("[x] Received %r" % body)
    print("Simulate processing...")
    time.sleep(2)
    print("Simulated processing finished")

# set up subscription on the queue
channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback, queue='hello', no_ack=True)

print('[*] Waiting for messages. To exit press CTRL+C')

# start consuming (blocks)
try:
    channel.start_consuming()
except KeyboardInterrupt:
    channel.stop_consuming()
connection.close()
