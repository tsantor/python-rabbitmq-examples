import pika
import sys
import os


# Access AMQP_URL (fallback to localhost)
url = os.environ.get('AMQP_URL', 'amqp://guest:guest@localhost/%2F')
params = pika.URLParameters(url)
params.socket_timeout = 5

# connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', socket_timeout=5))
connection = pika.BlockingConnection(params)  # Connect to AMQP
channel = connection.channel()  # start a channel
channel.queue_declare(queue='hello')  # Declare a queue

# send a message
message = "Hello World!"
channel.basic_publish(exchange='', routing_key='hello', body=message,)
print('[x] Sent "%r" to consumer' % message)
connection.close()
