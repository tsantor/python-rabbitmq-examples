import pika
import sys
import os


# Access AMQP_URL (fallback to localhost)
url = os.environ.get('AMQP_URL', 'amqp://guest:guest@localhost/%2F')
params = pika.URLParameters(url)
params.socket_timeout = 5

connection = pika.BlockingConnection(params)  # Connect to AMQP
channel = connection.channel()  # start a channel

channel.exchange_declare(exchange='direct_logs', exchange_type='direct')

# send a message
severity = sys.argv[1] if len(sys.argv) > 2 else 'info'
message = ' '.join(sys.argv[2:]) or 'Hello World!'
channel.basic_publish(exchange='direct_logs', routing_key=severity, body=message,)
print("[x] Sent %r:%r" % (severity, message))
connection.close()


# python producer.py error "Run. Run. Or it will explode."
