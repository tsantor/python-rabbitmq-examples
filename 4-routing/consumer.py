import pika
import time
import os
import sys

# Access the AMQP_URL (fallback to localhost)
url = os.environ.get('AMQP_URL', 'amqp://guest:guest@localhost/%2F')
params = pika.URLParameters(url)

# connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
connection = pika.BlockingConnection(params)  # Connect to AMQP
channel = connection.channel()  # start a channel

channel.exchange_declare(exchange='direct_logs', exchange_type='direct')

result = channel.queue_declare(exclusive=True)
queue_name = result.method.queue

severities = sys.argv[1:]
if not severities:
    sys.stderr.write("Usage: %s [info] [warning] [error]\n" % sys.argv[0])
    sys.exit(1)

for severity in severities:
    channel.queue_bind(exchange='direct_logs',
                       queue=queue_name,
                       routing_key=severity)

# create a function which is called on incoming messages
def callback(channel, method, properties, body):
    print("[x] %r:%r" % (method.routing_key, body))

# set up subscription on the queue
# channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback, queue=queue_name, no_ack=True)

print('[*] Waiting for messages. To exit press CTRL+C')

# start consuming (blocks)
try:
    channel.start_consuming()
except KeyboardInterrupt:
    channel.stop_consuming()
connection.close()


# python consumer.py warning error > logs_from_rabbit.log
# python consumer.py info warning error
