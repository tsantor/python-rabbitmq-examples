import pika
import time
import os
import sys

# Access the AMQP_URL (fallback to localhost)
url = os.environ.get('AMQP_URL', 'amqp://guest:guest@localhost/%2F')
params = pika.URLParameters(url)

# connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
connection = pika.BlockingConnection(params)  # Connect to AMQP
channel = connection.channel()  # start a channel

channel.exchange_declare(exchange='topic_logs', exchange_type='topic')

result = channel.queue_declare(exclusive=True)
queue_name = result.method.queue

binding_keys = sys.argv[1:]
if not binding_keys:
    sys.stderr.write("Usage: %s [binding_key]...\n" % sys.argv[0])
    sys.exit(1)

for binding_key in binding_keys:
    channel.queue_bind(exchange='topic_logs',
                       queue=queue_name,
                       routing_key=binding_key)

# create a function which is called on incoming messages
def callback(channel, method, properties, body):
    print("[x] %r:%r" % (method.routing_key, body))

# set up subscription on the queue
# channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback, queue=queue_name, no_ack=True)

print('[*] Waiting for messages. To exit press CTRL+C')

# start consuming (blocks)
try:
    channel.start_consuming()
except KeyboardInterrupt:
    channel.stop_consuming()
connection.close()

'''
To receive all the logs run:

    python consumer.py "#"

To receive all logs from the facility "kern":

    python consumer.py "kern.*"

Or if you want to hear only about "critical" logs:

    python consumer.py "*.critical"

You can create multiple bindings:

    python consumer.py "kern.*" "*.critical"
'''
