import pika
import sys
import os


# Access AMQP_URL (fallback to localhost)
url = os.environ.get('AMQP_URL', 'amqp://guest:guest@localhost/%2F')
params = pika.URLParameters(url)
params.socket_timeout = 5

connection = pika.BlockingConnection(params)  # Connect to AMQP
channel = connection.channel()  # start a channel

channel.exchange_declare(exchange='topic_logs', exchange_type='topic')

# send a message
routing_key = sys.argv[1] if len(sys.argv) > 2 else 'anonymous.info'
message = ' '.join(sys.argv[2:]) or 'Hello World!'
channel.basic_publish(exchange='topic_logs', routing_key=routing_key, body=message,)
print("[x] Sent %r:%r" % (routing_key, message))
connection.close()

'''
And to emit a log with a routing key "kern.critical" type:

    python producer.py "kern.critical" "A critical kernel error"
'''
